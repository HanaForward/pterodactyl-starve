FROM ubuntu:18.04

MAINTAINER Hana, <ooplay@ooplay.cn>

RUN dpkg --add-architecture i386 && \
    apt update && apt-get upgrade -y && \
    apt install -y lib32stdc++6 lib32gcc1 libcurl4-gnutls-dev:i386 && \
    useradd -d /home/container -m container
		
USER container
ENV  USER=container HOME=/home/container

WORKDIR /home/container

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]
