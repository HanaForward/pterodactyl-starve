#!/bin/bash
sleep 10

cd /home/container

ulimit -n 4096

# Update Rust Server
./steam/steamcmd.sh +login anonymous +force_install_dir /home/container +app_update 343050 +quit


# Replace Startup Variables
MODIFIED_STARTUP=`eval echo $(echo ${STARTUP} | sed -e 's/{{/${/g' -e 's/}}/}/g')`


echo ":/home/container$ ${MODIFIED_STARTUP}"

cd /home/container/bin

# Run the Server
${MODIFIED_STARTUP}

# Run the Server
echo "服务器退出了,如果是管理操作的请忽略此消息!"